/**
* ===============================================================================
* ARQUIVO............: Banheiro.java
* DESCRICAO..........: Codigo fonte correspondente a implementacao de um objeto
* 					   Banheiro, em que pessoas de generos diferentes tentam
* 					   acessa-lo, gerenciando sempre de acordo com sua capacidade 
* 					   e genero que esta utilizando o banheiro, buscando que
* 					   que todos consigam usa-lo corretamente
* AUTOR..............: Pedro Emerick (p.emerick@live.com)
* 					   Valmir Correa (valmircorrea96@outlook.com)
* CRIADO EM..........: 28/05/2018
* MODIFICADO EM......: 30/05/2018
* ===============================================================================
*/

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

/**
 * Classe em que pessoas de generos diferentes (Masculino ou Feminino) tentam
 * acessa-lo, gerenciando sempre de acordo com sua capacidade 
 * e genero que esta utilizando o banheiro, buscando que
 * que todos consigam usa-lo corretamente
 *
 */
public class Banheiro {
	// Capacidade de pessoas no banheiro
	private int capacidade;
	// Lista com as pessoas que estao utilizando o banheiro
	private ArrayList <Pessoa> banheiro;
	// Semaforos para gerenciar o uso do banheiro corretamente
	private Semaphore semaforo_geral;
	private Semaphore semaforo_add_rm;
	private Semaphore semaforo_max_genero;
	// Numero maximo em que pessoas do mesmo genero podem acessar o banheiro em sequencia
	private final int num_max_genero;
	// Contagem de pessoas do mesmo genero que acessaram o banheiro em sequencia
	private int cont_genero;
	// Tempo (em milisegundos) maximo que uma pessoa pode ficar no banheiro
	private long tempo_max;
	// Genero das pessoas que estao utilizando o banheiro
	private String genero;
	
	/**
	 * Construtor parametrizado
	 * @param capacidade Capacidade de pessoas no banheiro
	 * @param num_max_genero Numero maximo em que pessoas do mesmo genero podem 
	 * 						 acessar o banheiro
	 * @param tempo_max Tempo (em milisegundos) maximo que uma pessoa pode ficar no banheiro
	 */
	public Banheiro(int capacidade, int num_max_genero, long tempo_max) {
		this.capacidade = capacidade;
		this.banheiro = new ArrayList <Pessoa> ();
		this.semaforo_geral = new Semaphore (capacidade, true);
		this.semaforo_add_rm = new Semaphore (1, true);
		this.semaforo_max_genero = new Semaphore (1, true);
		this.num_max_genero = num_max_genero;
		this.tempo_max = tempo_max;
		this.cont_genero = num_max_genero;
		this.genero = null;
	}
	
	/**
	 * Metodo para uma pessoa entrar no banheiro e poder utiliza-lo
	 * @param p Pessoa que deseja entrar no banheiro
	 */
	public void entrarBanheiro (Pessoa p) {
		/* Caso tenha vaga no semaforo, ele entrara, senao entrara na fila 
		para acessar o recurso, quem tem politica FIFO */
		try {
			semaforo_geral.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		// Adiciona a pessoa na lista de pessoas que estao no banheiro
		try {
			semaforo_add_rm.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		banheiro.add(p);
		
		System.out.println("Entrou: [" + p.getNome() + "/" 
				+ p.getGenero() + "]  ||  BANHEIRO: " 
				+ this.banheiro.toString());
		
		semaforo_add_rm.release();
	}
	
	/**
	 * Metodo para uma pessoa sair do banheiro apos utiliza-lo
	 * @param p Pessoa que deseja sair do banheiro
	 */
	public void sairBanheiro (Pessoa p) {
		
		/* Remove a pessoa da lista de pessoas que estao no banheiro,
		e libera o banheiro para o proximo da fila */
		try {
			semaforo_add_rm.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		banheiro.remove(p);
		
		System.out.println("Saiu: [" + p.getNome() + "/" + 
				p.getGenero() + "]  ||  BANHEIRO: " + 
				banheiro.toString());
		
		semaforo_add_rm.release();
		
		semaforo_geral.release();		
	}

	
	/**
	 * Metodo que verifica se o banheiro esta vazio ou nao
	 * @return Se o banheiro esta vazio ou nao
	 */
	public boolean isEmpty () {
		return semaforo_geral.availablePermits() == capacidade;
	}
	
	/**
	 * Metodo que diz se uma pessoa pode entrar ou nao no banheiro 
	 * (ou fila se o banheiro esiver cheio)
	 * @param p Pessoa que deseja verificar
	 * @return Se a pessoa pode entrar ou nao
	 */
	public boolean possoEntrar (Pessoa p) {
		// Entra no semaforo pois ira fazer modificacoes no banheiro
		try {
			semaforo_max_genero.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		/* Se ainda pode entrar pessoas do mesmo genero, o banheiro nao esteja vazio,
		e a pessoa tenha o mesmo genero das pessoas do banheiro ela podera acessar 
		o banheiro */
		if (this.cont_genero >= 2 && !isEmpty() && this.genero == p.getGenero()) {
			
			this.cont_genero -= 1;
			semaforo_max_genero.release();
			
			return true;
		/* Senao se o banheiro estiver vazio, podera entrar */
		} else if (isEmpty()) {
			// Zera o contador e muda o genero das pessoas que estao no banheiro
			this.cont_genero = num_max_genero;
			this.genero = p.getGenero();
			
			semaforo_max_genero.release();
			
			return true;
		// Senao, nao podera entrar
		} else {
			semaforo_max_genero.release();
			
			return false;
		}
	}

	/**
	 * Metodo que verifica se uma pessoa esta usando o banheiro
	 * @param p Pessoa que deseja verificar se esta usando
	 * @return Se esta ou nao usando o banheiro
	 */
	public boolean estaUsando (Pessoa p) {
		return banheiro.contains(p);
	}

	/**
	 * Metodo que retorna a lista com as pessoas que estao no banheiro
	 * @return Lista com as pessoas que estao no banheiro
	 */
	public ArrayList<Pessoa> getBanheiro() {
		return banheiro;
	}

	/**
	 * Metodo que retorna o tempo (em milisegundos) maximo 
	 * que uma pessoa pode ficar no banheiro
	 * @return tempo (em milisegundos) maximo que uma pessoa pode ficar no banheiro
	 */
	public long getTempo_max() {
		return tempo_max;
	}
	
}

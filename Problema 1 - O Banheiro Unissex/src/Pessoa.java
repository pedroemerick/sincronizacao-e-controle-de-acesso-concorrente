/**
* ===============================================================================
* ARQUIVO............: Pessoa.java
* DESCRICAO..........: Codigo fonte correspondente a implementacao de uma thread 
* 					   o tipo Pessoa, que realiza o uso de um banheiro unissex,
* 					   compartilhado com outras threads do mesmo tipo, 
* 					   por extensao da classe java.lang.Thread
* AUTOR..............: Pedro Emerick (p.emerick@live.com)
* 					   Valmir Correa (valmircorrea96@outlook.com)
* CRIADO EM..........: 28/05/2018
* MODIFICADO EM......: 30/05/2018
* ===============================================================================
*/

/**
 * Classe que realiza o uso de um banheiro unissex,
 * compartilhado com outras threads do mesmo tipo
 */
public class Pessoa extends Thread {
	// Genero da pessoa (Masculino ou Feminino)
	private String genero;
	// Banheiro unissex compartilhado
	private Banheiro banheiro;
	// Tempo que ira ficar no banheiro
	private long tempo;
	// Nome da Pessoa
	private String nome;
	// Diz se ja usou ou nao o banheiro
	private boolean useiBanheiro;
	
	/**
	 * Construtor parametrizado
	 * @param genero Genero da pessoa (Masculino ou Feminino)
	 * @param banheiro Banheiro unissex compartilhado
	 * @param tempo Tempo que ira ficar no banheiro
	 * @param nome Nome da Pessoa
	 */
	public Pessoa(String genero, Banheiro banheiro, long tempo, String nome) {
		super();
		this.genero = genero;
		this.banheiro = banheiro;
		this.tempo = tempo;
		this.nome = nome;
		this.useiBanheiro = false;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 * 
	 * Sobrescrita do metodo run da classe java.lang.Thread;
	 * Fica tentando utilizar o banheiro e apos conseguir utilizar, e encerrada
	 */
	@Override
	public void run () {
		System.out.println("Tentando usar o banheiro: " + this.nome + " / " + this.genero);
		
		// Fica tentando utilizar o banheiro ate que consiga
		while (!useiBanheiro) {
			if (banheiro.possoEntrar(this)) {
				usarBanheiro();
			}
		}
	}
	
	
	/**
	 * Metodo em que a pessoa usa o banheiro, fica um determinado tempo,
	 * e depois sai de la
	 */
	public void usarBanheiro () {

		banheiro.entrarBanheiro(this);
		
		//System.out.println("Entrou: [" + this.nome + "/" + this.genero + "]");
		
		/* Verifica se o seu tempo de uso, nao e maior que o perimtido,
		caso seja, utiliza o banheiro com o tempo maximo permitido */
		long tempo_uso = this.tempo;
		
		if (tempo_uso > banheiro.getTempo_max())
			tempo_uso = banheiro.getTempo_max();
		
		// Faz o uso do banheiro, apenas dando um sleep na thread
		try {
			Thread.sleep(tempo_uso);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		// Modifica o valor da variavel, dizendo assim que ja utilizou o banheiro
		useiBanheiro = true;
		banheiro.sairBanheiro(this);
	}

	/**
	 * Metodo que retorna o genero da pessoa
	 * @return Genero da pessoa
	 */
	public String getGenero() {
		return genero;
	}

	/**
	 * Metodo que retorna o tempo em que a pessoa utilizara o banheiro
	 * @return Tempo em que a pessoa utilizara o banheiro
	 */
	public long getTempo() {
		return tempo;
	}

	/**
	 * Metodo que retorna o nome da pessoa
	 * @return Nome da pessoa
	 */
	public String getNome() {
		return nome;
	}

	/* (non-Javadoc)
	 * @see java.lang.Thread#toString()
	 * 
	 * Sobrescrita do metodo toString da classe java.lang.Thread;
	 */
	@Override
	public String toString () {
		return nome + "/" + genero;
	}
	
}

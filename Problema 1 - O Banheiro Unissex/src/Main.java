/**
* ===============================================================================
* ARQUIVO............: Main.java
* DESCRICAO..........: Codigo fonte principal para a criacao e execucao de threads,
* 					   do tipo Pessoa
* AUTOR..............: Pedro Emerick (p.emerick@live.com)
* 					   Valmir Correa (valmircorrea96@outlook.com)
* CRIADO EM..........: 28/05/2018
* MODIFICADO EM......: 30/05/2018
* ===============================================================================
*/

import java.util.ArrayList;
import java.util.Random;

public class Main {
	/**
	 * Capacidade de pessoas no banheiro
	 */
	public static final int CAPACIDADE_BANHEIRO = 4;
	/**
	 * Numero maximo em que pessoas do mesmo genero podem acessar o banheiro 
	 * em sequencia
	 */
	public static final int MAX_SEQ_GENERO = 8;
	/**
	 * Tempo (em milisegundos) maximo que uma pessoa pode ficar no banheiro
	 */
	public static final long TEMPO_MAXIMO_USO = 4000;
	/**
	 * Numero de pessoas (threads) que irao utilizar o banheiro
	 */
	public static final int NUMERO_THREADS_PESSOAS = 15;

	public static void main(String[] args) {
		
		System.out.println("BANHEIRO UNISSEX ABERTO");
		System.out.println("CAPACIDADE: " + CAPACIDADE_BANHEIRO);
		System.out.println("TEMPO MAXIMO DE USO: " + TEMPO_MAXIMO_USO);
		System.out.println("NUMERO MAXIMO DE PESSOAS DO MESMO GENERO EM SEQUENCIA: " 
							+ MAX_SEQ_GENERO + "\n");
		
		// Criacao do banheiro unissex compartilhado
		Banheiro banheiro = new Banheiro (CAPACIDADE_BANHEIRO, MAX_SEQ_GENERO, 
											TEMPO_MAXIMO_USO);
		
		// Criacao das pessoas (threads)
		ArrayList <Pessoa> pessoas = new ArrayList <Pessoa> ();
		
		for (int ii = 1; ii <= NUMERO_THREADS_PESSOAS; ii++) {
			/* Se o numero aleatorio entre 0 e 10, for divisivel por 2,
			sera iniciada uma pessoa do genero masculino, caso contrario,
			do genero feminino */
			if ((new Random().nextInt(10) % 2) == 0) {
				pessoas.add(new Pessoa ("Masculino", banheiro, 
							(long) new Random().nextInt(5000), ("Joao " + ii)));
			} else {
				pessoas.add(new Pessoa ("Feminino", banheiro, 
							(long) new Random().nextInt(5000), ("Maria " + ii)));
			}
		}
		
		// Inicia todas as pessoas (threads)
		for (Pessoa p : pessoas) {
			p.start();
		}
		
		// Espera por todas as pessoas (threads)
		for (Pessoa p : pessoas) {
			try {
				p.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("\nBANHEIRO UNISSEX FECHADO");
	}

}

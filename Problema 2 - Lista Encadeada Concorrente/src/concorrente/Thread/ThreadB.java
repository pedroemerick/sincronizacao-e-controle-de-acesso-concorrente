/**
* ===============================================================================
* ARQUIVO............: ThreadB.java
* DESCRICAO..........: Codigo fonte correspondente a implementacao de uma thread 
* 					   o tipo B, que realiza operações de busca sobre a lista, 
* 					   por extensao da classe java.lang.Thread
* AUTOR..............: Pedro Emerick (p.emerick@live.com)
* 					   Valmir Correa (valmircorrea96@outlook.com)
* CRIADO EM..........: 15/05/2018
* MODIFICADO EM......: 30/05/2018
* ===============================================================================
*/

package concorrente.Thread;

/**
 * Classe que representa a thread do tipo B, que que realiza operações de 
 * busca sobre a lista 
 */
public class ThreadB extends Thread {
	// Objeto do tipo lista
	private Lista lista;
	// Valor inteiro a ser buscado na lista
	private int busca;

	/**
	 * Construtor parametrizado
	 * @param nome Nome para a thread
	 * @param lista Objeto do tipo lista, com acesso compartilhado
	 * @param busca Valor a ser buscado na lista
	 */
	public ThreadB(String nome, Lista lista, int busca) {
		super(nome);
		this.lista = lista;
		this.busca = busca;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 * 
	 * Sobrescreve o metodo run de Thread;
	 * Printa que a thread foi iniciada, e chama o metodo buscar do objeto lista
	 */
	@Override
	public void run () {
		System.out.println (Thread.currentThread().getName() + ": Criada thread para busca");
		lista.buscar (busca);	
	}
	
}

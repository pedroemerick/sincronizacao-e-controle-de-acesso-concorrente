/**
* ===============================================================================
* ARQUIVO............: ThreadR.java
* DESCRICAO..........: Codigo fonte correspondente a implementacao de uma thread 
* 					   o tipo R, que realiza operações de remoção de itens a 
* 					   partir de qualquer posição da lista, 
* 					   por extensao da classe java.lang.Thread
* AUTOR..............: Pedro Emerick (p.emerick@live.com)
* 					   Valmir Correa (valmircorrea96@outlook.com)
* CRIADO EM..........: 15/05/2018
* MODIFICADO EM......: 30/05/2018
* ===============================================================================
*/

package concorrente.Thread;

/**
 * Classe que representa a thread do tipo R, que realiza operações de remoção 
 * de itens a partir de qualquer posição da lista
 */
public class ThreadR extends Thread {
	// Objeto do tipo lista
	private Lista lista;
	// Valor a ser removido da lista
	private int valor;

	/**
	 * Construtor parametrizado
	 * @param nome Nome para a thread
	 * @param lista Objeto do tipo lista, com acesso compartilhado
	 * @param valor Valor a ser removido da lista
	 */
	public ThreadR(String nome, Lista lista, int valor) {
		super(nome);
		this.lista = lista;
		this.valor = valor;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 * 
	 * Sobrescreve o metodo run de Thread;
	 * Printa que a thread foi iniciada, e chama o metodo remover do objeto lista
	 */
	@Override
	public void run () {
		System.out.println (Thread.currentThread().getName() + ": Criada thread para remocao");
		lista.remover(valor);
	}
	
}

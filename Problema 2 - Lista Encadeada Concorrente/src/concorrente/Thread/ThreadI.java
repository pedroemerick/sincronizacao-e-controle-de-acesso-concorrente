/**
* ===============================================================================
* ARQUIVO............: ThreadI.java
* DESCRICAO..........: Codigo fonte correspondente a implementacao de uma thread 
* 					   o tipo I, que realiza operações de inserção de itens no final 
* 					   da lista, por extensao da classe java.lang.Thread
* AUTOR..............: Pedro Emerick (p.emerick@live.com)
* 					   Valmir Correa (valmircorrea96@outlook.com)
* CRIADO EM..........: 15/05/2018
* MODIFICADO EM......: 30/05/2018
* ===============================================================================
*/

package concorrente.Thread;

/**
 * Classe que representa a thread do tipo I, que que realiza operações de 
 * inserção de itens no final da lista 
 */
public class ThreadI extends Thread {
	// Objeto do tipo lista 
	private Lista lista;
	// Valor para insercao na lista
	private int valor;

	/**
	 * Construtor parametrizado
	 * @param nome Nome para a thread
	 * @param lista Objeto do tipo lista, com acesso compartilhado
	 * @param valor Valor para insercao na lista
	 */
	public ThreadI(String nome, Lista lista, int valor) {
		super(nome);
		this.lista = lista;
		this.valor = valor;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 * 
	 * Sobrescreve o metodo run de Thread;
	 * Printa que a thread foi iniciada, e chama o metodo inserir do objeto lista
	 */
	@Override
	public void run () {
		System.out.println (Thread.currentThread().getName() + ": Criada thread para insercao");
		lista.inserir (valor);
	}
	
}

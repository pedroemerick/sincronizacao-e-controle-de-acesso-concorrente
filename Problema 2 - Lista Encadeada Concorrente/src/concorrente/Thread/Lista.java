/**
* ===============================================================================
* ARQUIVO............: Lista.java
* DESCRICAO..........: Codigo fonte correspondente a implementacao de um objeto
* 					   Lista, que realiza a sincronizacao das operacoes de insercao,
* 					   remocao e busca de itens em um lista encadeada, realizadas 
* 					   por tres tipos de threads
* AUTOR..............: Pedro Emerick (p.emerick@live.com)
* 					   Valmir Correa (valmircorrea96@outlook.com)
* CRIADO EM..........: 15/05/2018
* MODIFICADO EM......: 30/05/2018
* ===============================================================================
*/

package concorrente.Thread;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Classe que realiza a sincronizacao das operacoes de insercao,
 * remocao e busca de itens em um lista encadeada, realizadas 
 * por tres tipos de threads
 */
public class Lista {
	// Lista encadeada de inteiros para insercao, remocao e busca de itens
	private LinkedList <Integer> lista;
	// Bloqueios explicitos utilizados na solucao do problema
	private Lock bloqueio_insercao;
	private Lock bloqueio_remocao;
	private Lock bloqueio_busca;
	// Variavel condicional do bloqueio_remocao
	private Condition cond_rm;
	// Contador de threads realizando busca
	private int cont_busca;
	
	/**
	 * Construtor padrao
	 */
	public Lista() {
		this.lista = new LinkedList <Integer> ();
		this.bloqueio_insercao = new ReentrantLock();
		this.bloqueio_remocao = new ReentrantLock();
		this.bloqueio_busca = new ReentrantLock();
		this.cond_rm = bloqueio_remocao.newCondition();
		this.cont_busca = 0;
	}
	
	/**
	 * Metodo que inseri um inteiro no fim da lista encadeada
	 * @param add Valor a ser inserido na lista
	 */
	public void inserir (int add) {
		
		// Apenas para uma visualizacao melhor da saida do programa
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		/* Realiza o bloqueio da insercao para garantir que apenas uma insercao
		sera realizada por vez, permitindo que buscas sejam realizadas simutaneamente */
		bloqueio_insercao.lock ();
		try {
			System.out.println (Thread.currentThread().getName() + ": <--- Iniciando insercao");
			
			// Realiza a insercao do valor no fim da lista
			lista.add(add);
			
			System.out.println(Thread.currentThread().getName() + ": Inseri (" + add + ")");
			System.out.println("LISTA: " + lista.toString());
			
			// Apenas para uma visualizacao melhor da saida do programa
			Thread.sleep(1500);

		} catch (InterruptedException e) {
			// Pode ser causado pelo sleep
			e.printStackTrace();
		} finally {
			System.out.println (Thread.currentThread().getName() + ": ---> Insercao finalizada");
			bloqueio_insercao.unlock();
		}
	}
	
	/**
	 * Metodo que remove um elemento da lista encadeada, caso a lista o contenha
	 * @param rm Valor a ser removido da lista
	 */
	public void remover (int rm) {
		
		// Apenas para uma visualizacao melhor da saida do programa
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		/* Realiza o bloqueio da insercao, remocao e busca para garantir que apenas 
		uma insercao sera realizada por vez */
		bloqueio_insercao.lock ();
		bloqueio_busca.lock();
		bloqueio_remocao.lock();
	
		try {	
			// Certifica de que nao ha nenhuma busca acontecendo, se tiver, ira dormir
			while (cont_busca > 0) {
				cond_rm.await();
			}
			
			System.out.println (Thread.currentThread().getName() + ": <--- Iniciando remocao");
			
			// Apenas para uma visualizacao melhor da saida do programa
			Thread.sleep(1500); 				
			
			// Verifica se a lista esta vazia
			if (!lista.isEmpty()) {
				/* Se a lista conter o valor, entao e removido,
				senao mostra uma mensagem de que nao existe este valor na lista */		
				if (lista.remove((Integer) rm)) {
					System.out.println(Thread.currentThread().getName() + 
										": Removi (" + rm + ")");
				} else {
					System.out.println(Thread.currentThread().getName() + 
							": Valor nao encontrado para remocao (" + rm + ")");
				}
				
				System.out.println ("LISTA: " + lista.toString());
			} else {
				System.out.println(Thread.currentThread().getName() + ": Lista vazia, sem elementos para remover");
			}

		} catch (InterruptedException e) {
			// Pode ser causado pelo await ou sleep
			e.printStackTrace();
		} finally {
			System.out.println (Thread.currentThread().getName() + ": ---> Remocao finalizada");
			
			bloqueio_busca.unlock();
			bloqueio_insercao.unlock();
			bloqueio_remocao.unlock();
		}
	}
	
	/**
	 * Metodo que busca um valor na lista encadeada
	 * @param busca Valor inteiro a ser buscado
	 */
	public void buscar (int busca) {

		// Apenas para uma visualizacao melhor da saida do programa
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		/* Realiza o bloqueio e desbloqueio da busca em sequencia, apenas para garantir
		que nao entrara remocao neste instante */
		bloqueio_busca.lock();
		bloqueio_busca.unlock();
		
		// Incrementa no contador de threads realizando busca
		bloqueio_remocao.lock();
		try {
			cont_busca += 1;
		} finally {
			bloqueio_remocao.unlock();
		}
		
		// Realiza a busca e apenas printa se foi encontrado ou nao o valor
		System.out.println (Thread.currentThread().getName() + ": <--- Iniciando busca");
		if (lista.contains(busca)) {
			System.out.println(Thread.currentThread().getName() + ": Valor encontrado (" + busca + ")");
		} else {
			System.out.println(Thread.currentThread().getName() + ": Valor nao encontrado (" + busca + ")");
		}
		
		// Decrementa no contador de threads realizando busca
		bloqueio_remocao.lock();
		try {
			cont_busca -= 1;
			
			if (cont_busca == 0) {
				cond_rm.signalAll();
			}
		} finally {
			System.out.println (Thread.currentThread().getName() + ": ---> Busca finalizada");
			bloqueio_remocao.unlock();
		}
		
		// Apenas para uma visualizacao melhor da saida do programa
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodo que retorna a lista encadeada
	 * @return LinkedList - Lista encadeada
	 */
	public LinkedList<Integer> getLista() {
		return lista;
	}

}

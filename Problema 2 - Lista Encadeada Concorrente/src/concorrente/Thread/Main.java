/**
* ===============================================================================
* ARQUIVO............: Main.java
* DESCRICAO..........: Codigo fonte principal para a criacao e execucao de threads,
* 					   do tipo I (insercao), R (remocao) e B (busca)
* AUTOR..............: Pedro Emerick (p.emerick@live.com)
* 					   Valmir Correa (valmircorrea96@outlook.com)
* CRIADO EM..........: 15/05/2018
* MODIFICADO EM......: 30/05/2018
* ===============================================================================
*/

package concorrente.Thread;

import java.util.ArrayList;
import java.util.Random;

public class Main {
	/**
	 * Numero de threads do tipo I (insercao) que serao criadas
	 */
	public static final int NUM_THREADS_I = 11;
	/**
	 * Numero de threads do tipo R (remocao) que serao criadas
	 */
	public static final int NUM_THREADS_R = 12;
	/**
	 * Numero de threads do tipo B (busca) que serao criadas
	 */
	public static final int NUM_THREADS_B = 22;
	
	public static void main(String[] args) throws InterruptedException {
		
		// Criacao do objeto do tipo Lista, que sera compartilhado
		Lista lista = new Lista ();
		// Variavel utilizada para gerar numeros randomicos
		Random num = new Random ();
		// ArrayList que armazena as threads do tipo I
		ArrayList <Thread> threadsI = new ArrayList <Thread> ();
		// ArrayList que armazena as threads do tipo R
		ArrayList <Thread> threadsR = new ArrayList <Thread> (); 
		// ArrayList que armazena as threads do tipo B
		ArrayList <Thread> threadsB = new ArrayList <Thread> ();
		
		// Criacao das threads de cada um dos tipos
		for (int ii = 1; ii <= NUM_THREADS_I; ii++) {
			threadsI.add(new ThreadI ("TI/" + ii + " - INSERCAO", lista, num.nextInt(100)));
		}

		for (int ii = 1; ii <= NUM_THREADS_R; ii++) {
			threadsR.add(new ThreadR ("TR/" + ii + " - REMOCAO", lista, num.nextInt(100)));
		}

		for (int ii = 1; ii <= NUM_THREADS_B; ii++) {
			threadsB.add(new ThreadB ("TB/" + ii + " - BUSCA", lista, num.nextInt(100)));
		}
			
		// Threads de cada tipo passam ao estado executavel
		for (Thread threadi : threadsI) {
			threadi.start();
		}
	
		for (Thread threadr : threadsR) {
			threadr.start();
		}
		
		for (Thread threadb : threadsB) {
			threadb.start();
		}
		
		// Espera por todas as threads
		for (Thread threadi : threadsI) {
			threadi.join();
		}

		for (Thread threadr : threadsR) {
			threadr.join();
		}

		for (Thread threadb : threadsB) {
			threadb.join();
		}
		
		// Printa que foi concluido com sucesso e o a lista final
		System.out.println("\nTHREADS CONCLUIDAS COM SUCESSO !!!");
		System.out.println("LISTA: " + lista.getLista().toString());
	}
}
